package com.loria.kiwi.Start;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.loria.kiwi.Controller.ExistingConfigurationRecapController;
import com.loria.kiwi.Controller.PostConfigurationStepsController;
import com.loria.kiwi.Controller.RecapPostConfigurationController;
import com.loria.kiwi.Controller.PostRuningTestController;
import com.loria.kiwi.Model.Configuration;
import com.loria.kiwi.Model.Scenario;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * @author Chahrazed LABBA
 *
 */
public class StartMain extends Application {
	private Stage primaryStage;
	private BorderPane rootLayout;
	public static Scenario scenario = new Scenario();
	public static Scenario scenario1 = new Scenario();
	public static Configuration config = new Configuration();
	public static Configuration config1 = new Configuration();
	public String path = "/com/loria/kiwi/Controller/";

	//public static void main(String[] args) {
		//launch(args);
	//}
	public void start(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("-- Performance Test Framework --");
		initRootLayout();
		intialInterface();

	}

	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(StartMain.class.getResource(path + "RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void intialInterface() {
		try {

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(StartMain.class.getResource(path + "WecomePage.fxml"));
			AnchorPane scenario = (AnchorPane) loader.load();
			rootLayout.setCenter(scenario);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showPostConfigurationSteps(Configuration configuration) {

		scenario = configuration.getScenario();
		try {

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(StartMain.class.getResource(path + "PostConfigurationSteps.fxml"));
			AnchorPane anchor = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Configuration Steps ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			PostConfigurationStepsController controller = loader.getController();
			controller.interfaceManagement(scenario);
			Scene scene = new Scene(anchor);
			dialogStage.setScene(scene);
			dialogStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void showPostRecapConfiguration(Configuration configuration) {
		configuration.setScenario(scenario);
		System.out.println(configuration.getprotocol());
		try {

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(StartMain.class.getResource(path + "RecapPostConfiguration.fxml"));
			AnchorPane anchor = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Recap Configuration ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(anchor);
			dialogStage.setScene(scene);
			RecapPostConfigurationController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.printConfiguration(configuration);
			dialogStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void runPostTest(Configuration configuration) {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(StartMain.class.getResource(path + "PostGetRunningTest.fxml"));
			AnchorPane anchor = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Run the Configuration ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(anchor);
			dialogStage.setScene(scene);
			PostRuningTestController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.showprogressBar(configuration);
			dialogStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public boolean showRecapForExistingConfig(Scenario scenari) {
		System.out.println("The path for the File is " + scenari.getpathconfig());
		try {

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(StartMain.class.getResource(path+"ExistingConfigRecap.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Configuration Summary ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the config into the controller.
			ExistingConfigurationRecapController controller1 = loader.getController();
			controller1.setDialogStage(dialogStage);
			controller1.getConfiguration(scenari.getpathconfig());
			
			dialogStage.show();

			return controller1.isOkClicked();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void saveTextToFile(Configuration config, File file) {
		String COMMA_DELIMITER = ";";
		String NEW_LINE_SEPARATOR = "\n";
		String FILE_HEADER = "requestType;postscenarioType;NumberTestRuns;breakTimeRuns;timeunit1;NumberRequest;testDuration;timeunit2;breakTimmeRequest;timeunit3;NumberStatement;timegenerationway;domain;path;port;userNumber;protocol;URL;login;Password;statementgenerationway;range;constant";
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);

			// Write the CSV file header
			fileWriter.append(FILE_HEADER.toString());

			// Add a new line separator after the header
			fileWriter.append(NEW_LINE_SEPARATOR);

			// Write a new student object list to the CSV file

			fileWriter.append(config.getScenario().getrequestType());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.getScenario().get_postscenarioType());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getnumberTestRuns()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getbreakTimeRuns()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.gettimeunit1()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getnumberRequest()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf((config.gettestduration())));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf((config.gettimeunit2())));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.gettimeobj().gettimevalue()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.gettimeobj().gettimeunit());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getstatementNumber()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.gettimeobj().getgenerationway());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.getdomain());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.getpath());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getport()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getuserNumber()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.getprotocol());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.geturl());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.getlogin());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.getpassword());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(config.getstatementgenerationWay());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getrange()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(config.getconstant()));
			fileWriter.append(NEW_LINE_SEPARATOR);

		} catch (Exception e) {
			System.out.println("Error while writing in the Csv !");
			e.printStackTrace();
		} finally {

			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}

		}

	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	// Not yet implemented
	public void showBothConfigurationSteps(Configuration configuration, Configuration configuration1) {

	}

	// not yet implemented
	public void showGetConfigurationSteps(Configuration configuration) {

	}

	public void showGetRecapConfiguration(Configuration configuration) {

	}

	public void showBothRecapConfiguration(Configuration configuration, Configuration configuration1) {

	}

	public void runBothConfig(Configuration configuration, Configuration configuration1) {

	}

	public void runGetTest(Configuration configuration) {

	}

}
