package com.loria.kiwi.Controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.loria.kiwi.Model.Configuration;
import com.loria.kiwi.Model.Scenario;
import com.loria.kiwi.Model.Timeobject;
import com.loria.kiwi.Start.StartMain;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ExistingConfigurationRecapController {

	@FXML
	private Label requestType;
	private String requestType1;
	@FXML
	private Label scenarioType;
	private String scenarioType1;
	@FXML
	private Label testRuns;
	private int testRuns1;
	@FXML
	private Label breakTime;
	private int breakTime1;
	@FXML
	private Label requestNumber;
	private int requestNumber1;
	@FXML
	private Label timeInterval;
	private int timeInterval1;
	@FXML
	private Label statementNumber;
	private int statementNumber1;
	@FXML
	private Label domain;
	private String domain1;
	@FXML
	private Label port;
	private int port1;
	@FXML
	private Label userNumber;
	private int userNumber1;
	@FXML
	private Label login;
	private String login1;
	@FXML
	private Label path;
	private String path11;
	@FXML
	private Label protocol;
	private String protocol1;
	@FXML
	private Label url;
	private String url1;
	@FXML
	private Label password;
	private String password1;
	@FXML
	private Label testDuration;
	private String testDuration1;
	@FXML
	private Label timeUnitDuration;
	private String timeUnitDuration1;
	@FXML
	private Label timeRequest;
	private String timeRequest1;
	@FXML
	private Label timeUnitRequest;
	private String timeUnitRequest1;
	@FXML
	private Label timeGenerationWay;
	private String timeGenerationWay1;
	@FXML
	private Label statementGenerationWay;
	private String statementGenerationWay1;

	private boolean okClicked = false;
	private Stage dialogStage;
	private Configuration configuration;
	private Scenario scenario;
	private StartMain mainApp = new StartMain();

	@FXML
	private void initialize() {

	}

	public void getConfiguration(String path1) {

		try (Stream<String> lines = Files.lines(Paths.get(path1))) {
			configuration = new Configuration();
			scenario = new Scenario();
			scenario.setpathconfig(path1);
			Timeobject time = new Timeobject();
			List<List<String>> values = lines.map(line -> Arrays.asList(line.split(";"))).skip(1)
					.collect(Collectors.toList());

			requestType.setText(values.get(0).get(0));

			scenarioType.setText(values.get(0).get(1));
			scenario.setpostscenarioType(values.get(0).get(1));
			configuration.setScenario(scenario);

			testRuns.setText(values.get(0).get(2));
			configuration.setnumberTestRuns(Integer.parseInt(values.get(0).get(2)));

			breakTime.setText(values.get(0).get(3));
			configuration.setbreakTimeRuns(Integer.parseInt(values.get(0).get(3)));

			timeInterval.setText(values.get(0).get(4));
			configuration.settimeunit1(values.get(0).get(4));

			if (Integer.parseInt(values.get(0).get(5)) == 0) {
				requestNumber.setText("None");
			} else {
				requestNumber.setText(values.get(0).get(5));
			}

			configuration.setnumberRequest(Integer.parseInt(values.get(0).get(5)));

			testDuration.setText(values.get(0).get(6));
			configuration.settestduration(Integer.parseInt(values.get(0).get(6)));

			timeUnitDuration.setText(values.get(0).get(7));
			configuration.settimeunit2(values.get(0).get(7));

			if (Integer.parseInt(values.get(0).get(8)) == 0) {
				timeRequest.setText("None");
				timeUnitRequest.setText("None");
			} else {
				timeRequest.setText(values.get(0).get(8));
				timeUnitRequest.setText(values.get(0).get(9));
			}

			time.settimevalue(Integer.parseInt(values.get(0).get(8)));
			time.settimeunit(values.get(0).get(9));

			if (Integer.parseInt(values.get(0).get(10)) == 0) {
				statementNumber.setText("None");
				statementGenerationWay.setText(values.get(0).get(20));
			} else {
				statementNumber.setText(values.get(0).get(10));
				statementGenerationWay.setText("None");
			}

			configuration.setstatementNumber(Integer.parseInt(values.get(0).get(10)));

			timeGenerationWay.setText(values.get(0).get(11));
			time.setgenerationway(values.get(0).get(11));
			configuration.settimeobj(time);

			domain.setText(values.get(0).get(12));
			configuration.setdomain(values.get(0).get(12));

			path.setText(values.get(0).get(13));
			configuration.setpath(values.get(0).get(13));

			port.setText(values.get(0).get(14));
			configuration.setport(Integer.parseInt(values.get(0).get(14)));

			userNumber.setText(values.get(0).get(15));
			configuration.setuserNumber(Integer.parseInt(values.get(0).get(15)));

			protocol.setText(values.get(0).get(16));
			configuration.setprotocol(values.get(0).get(16));

			url.setText(values.get(0).get(17));
			configuration.seturl(values.get(0).get(17));

			login.setText(values.get(0).get(18));
			configuration.setlogin(values.get(0).get(18));

			password.setText(values.get(0).get(19));
			configuration.setpassword(values.get(0).get(19));

			configuration.setstatementgenerationWay(values.get(0).get(20));

			configuration.setrange(Integer.parseInt(values.get(0).get(21)));
			configuration.setconstant(Integer.parseInt(values.get(0).get(22)));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void runconfig() throws IOException {

		mainApp.runPostTest(configuration);
		dialogStage.close();

	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public boolean isOkClicked() {
		return okClicked;
	}

	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

}
