# LOLA-meter

This tool allows to bechmark LRS in terms of reponse time , throughput and error rate. 
The tool is based on the use of the apache-jmeter java API.

## Getting started
 Maven is used to include all the required dependencies. 
The tool has been developed using the following technical environment : 
- Java version 1.8.0_172
- JavaFX 11
- apache-jemeter : 5.1.1
- maven version : 3.8.1
- Editor: Eclipse


## How to run the tool

- In a directory with the following structure: 
    - benchmarkframeworkProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar : the jar of the tool
    - report-output : create the final execution report in the format of html that displays a dashboard with all the results
    - report-template : the template for the generated dashboard
    - JsonFiles : contains the Json files of the xAPI statements (generated automatically according to the configuration)
    - jemeter.properties : a configuration file required to run jmeter 
    - reportgenerator.properties : required for the final report generation
    - saveservice.properties : jmeter properties file

To start the tool: 

`java -jar benchmarkframeworkProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar`

The detailed description of the diffrent scenarios ensured by the lola-meter can be found [here](https://hal.archives-ouvertes.fr/hal-02985541/document). 

Once the tool is launched, the user has the following interface: 

<img src="images/lm1.png" alt="drawing" width="600"/>.

The user can select between creating a new configuration or use an existing configuration. LOLA-meter allows saving a configuration in the format of csv file for a later use. 

For now only POST requests are supported. following, the scenario-1 for a POST request is shown. 

- scenario-1 is a load test: fixed request number, fixed statments number and fixed time interval that separates the sent of two successive requests. 

<img src="images/lm2.png" alt="drawing" width="600"/>.

- The interface to configurate the scenario-1. 

<img src="images/lm3.png" alt="drawing" width="600"/>.

- The first step is defining the number of runs (how many times to repeat the scenario) and define the time that seperates two successive runs. For exemple, define 2 test runs that are seperated by a time interval equal to 1 minute. In other words the system launchs the first run, once finish it waits 1 minute before launching the second scenario run. 

- The second step consists of configuarting information related to the request number, the time interval that seperates two successive requests and the number of statements sent in one request. For exemple, 20 requests per scenario run, 1 second between two successive requests and each request contains 100 statments (json body). 

- The third step consists in entering the LRS connection information. 

- Once done, the user needs to click on the button Finish. A summary of the scenario configuration is displayed and the user can save the configuration in a csv file for a later use. 

<img src="images/lm4.png" alt="drawing" width="600"/>.

- To start the scenario execution, click on Run Config. : the user is provided with an interface to display the execution progress. 

<img src="images/lm5.png" alt="drawing" width="600"/>.
<img src="images/lm6.png" alt="drawing" width="600"/>.

- Once the execution is finish, the user can check the execution summary by clisking on the button show Results. 

The navigator is launched and the user is provided with a summary of the execution.

<img src="images/lm7.png" alt="drawing" width="600"/>.













